.. pyReddit documentation master file, created by
   sphinx-quickstart on Tue Dec 22 22:57:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyReddit's documentation!
====================================
pyReddit is a terminal-based reader for RSS feeds from Reddit. There were several main goals I had in mind when writing PyReddit:

* Privacy
* Speed
* Customizability

While it is not done yet, I am working on adding color support for the program.

Adding Feeds
============

pyReddit reads your feeds from the `feeds.yaml` located in `~/.config/pyreddit`. Here is an example config file:

.. code-block:: yaml

    category: 
        Gaming:
            - title: Crusader Kings
              url: "https://old.reddit.com/r/CrusaderKings/search.rss?q=self%3Ayes&restrict_sr=on&include_over_18=on&sort=new&t=all"
            - title: Low Sodium Destiny
              url: "https://old.reddit.com/r/LowSodiumDestiny.rss"
        Programming:
            - title: Python
              url: "https://reddit.com/r/python/search.rss?q=self%3Ayes&restrict_sr=on&include_over_18=on&sort=relevance&t=all"
            - title: Programming
              url: "https://www.reddit.com/r/programming/search.rss?q=self%3Ayes&restrict_sr=on&include_over_18=on&sort=relevance&t=all"

In short, group each subreddit you want to read by a certain category. I find it easier to view what I want to this way.

Things To Do
============

- Enable caching to get posts more quickly.
- Enable user configurable colors.
- Enable opening in browser.
